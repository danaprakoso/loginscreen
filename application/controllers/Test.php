<?php

include "FCM.php";
include "Util.php";

class Test extends CI_Controller {
	
	public function fcm() {
		FCM::send_message("This is title", "This is body", "cwJTfJZ-Bu0:APA91bEo6oL_viLiXL4oB7K3gGchEOTJTm0rH1IIYlZ-xBsbw4bhcg5uFN4-GbJ6WJTiDunA-EhrxTJvu2bmHsifsPpVak8eVgKggTho7TIa6AInjQieF1IKcyJDMie6fa2O-F1LPIEO", array('type' => 'logout'));
	}
	
	private function send_email($to, $subject, $body) {
		$headers = "From: admin@redowl.web.id\r\n";
		$headers .= "Reply-To: " . $to . "\r\n";
		$headers .= "Content-Type: text/html; charset=UTF-8\r\n";
		mail($to, $subject, $body, $headers);
	}
	
	public function email() {
		echo "Sending email...";
		Util::send_email("danaos.apps@gmail.com", "This is subject", "This is <b>body</b>");
	}
	
	public function code_verification() {
		$email = $this->input->get('email');
		$code = Util::generateRandomNumber(6);
		Util::send_email($email, 'Kode verifikasi email Anda: ' . $code, 'Mohon masukkan 6-digit kode verifikasi berikut di kotak yang tersedia di aplikasi: <b>' . $code . '</b>');
		echo $code;
	}
	
	public function native_mail() {
		Util::send_email("danaoscompany@gmail.com", "This is subject", "This is body");
	}
	
	public function date() {
		echo strtotime('2021-07-03 11:41:00');
	}
}
