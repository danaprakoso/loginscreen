-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 11, 2020 at 03:04 PM
-- Server version: 10.3.25-MariaDB
-- PHP Version: 7.3.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u9305686_redowl`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `password` text DEFAULT NULL,
  `max_users` int(11) DEFAULT 5
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `email`, `password`, `max_users`) VALUES
(1, 'AliZon', 'lutfi.xtream@gmail.com', 'aliali', 914),
(6, 'Jets', 'wisardfolks@gmail.com', 'rana', 11),
(2, 'Admin 1', 'admin1@gmail.com', 'abc', 100),
(7, 'Ubay', 'nizhamch@gmail.com', 'siliwangi', 0),
(8, 'Iway', 'iway.siliwangi04@gmail.com', 'abcd', 5),
(9, 'Idris', 'darrell.axell@gmail.com', 'gojek123', 7),
(10, 'Irfan', 'irfaniqbale@gmail.com', 'balebool', 5),
(11, 'Nana', 'gnanawijaya1996@gmail.com', 'penang', 4),
(12, 'Trial', 'wosardfolks@gmail.com', 'trial', 1936),
(13, 'Daston', 'bogamamah@gmail.com', 'rizkinhp01', 3),
(14, 'Ranaa', 'rannnaa88@gmail.com', 'rana', 1998);

-- --------------------------------------------------------

--
-- Table structure for table `superadmin`
--

CREATE TABLE `superadmin` (
  `id` int(11) NOT NULL,
  `email` text DEFAULT NULL,
  `password` text DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `superadmin`
--

INSERT INTO `superadmin` (`id`, `email`, `password`) VALUES
(1, 'alizon.goroot@gmail.com', 'abcd'),
(2, 'superadmin1@gmail.com', 'abcd');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) DEFAULT 0,
  `first_name` text DEFAULT NULL,
  `last_name` text DEFAULT NULL,
  `phone` text DEFAULT NULL,
  `password` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `google_id` text DEFAULT NULL,
  `android_id` text DEFAULT NULL,
  `expiry` datetime DEFAULT NULL,
  `profile_completed` int(11) DEFAULT 0
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `admin_id`, `first_name`, `last_name`, `phone`, `password`, `email`, `google_id`, `android_id`, `expiry`, `profile_completed`) VALUES
(444, 10, NULL, NULL, '+6287871537124', 'tony', NULL, NULL, 'N2G47H', '2021-01-01 22:48:05', 0),
(4, 1, 'ali', 'goroot', '+628122979169', 'abcd', 'alizon.goroot@gmail.com', 'DkkwIb3lAaN6gASpT44DImcCjTF3', 'PKQ1.181121.001', '2020-12-30 12:00:18', 1),
(423, 1, NULL, NULL, '+6287882889731', 'drfb', NULL, NULL, 'NJH47F', '2020-12-27 12:27:44', 0),
(119, 1, NULL, NULL, '+6287788219160', 'anwar', NULL, NULL, 'PKQ1.190319.001', '2020-12-20 10:40:10', 0),
(443, 10, NULL, NULL, '+6285771232337', 'bela', NULL, NULL, 'MMB29M', '2021-01-01 17:32:27', 0),
(283, 6, NULL, NULL, '+6283898915241', 'jwiv', NULL, NULL, 'NRD90M', '2020-12-25 11:45:52', 0),
(100, 1, NULL, NULL, '+6282125242124', 'mamat', NULL, NULL, 'QP1A.190711.020', '2021-01-11 10:47:31', 0),
(99, 1, NULL, NULL, '+6281382121143', 'sandy', NULL, NULL, 'PKQ1.180917.001', '2020-12-10 18:11:58', 0),
(97, 1, NULL, NULL, '+6285156578365', 'putra', NULL, NULL, 'NMF26X', '2020-12-09 12:25:30', 0),
(95, 6, NULL, NULL, '+6289510309843', 'rana', NULL, NULL, 'QKQ1.190828.002', '2021-10-07 16:40:57', 0),
(52, 1, NULL, NULL, '+6281281319126', 'dhik', NULL, NULL, 'NRD90M', '2021-01-04 23:45:32', 0),
(449, 10, NULL, NULL, '+6287876604545', 'gembul', NULL, NULL, 'QKQ1.191014.001', '2021-01-02 22:37:55', 0),
(377, 6, NULL, NULL, '+6289677194662', 'lqef', NULL, NULL, 'NRD90M', '2020-12-16 18:27:36', 0),
(103, 1, NULL, NULL, '+628112233', 'mi', NULL, NULL, 'OPR1.170623.032', '2020-12-31 21:02:29', 0),
(314, 1, NULL, NULL, '+6282123454723', 'drki', NULL, NULL, 'R16NW', '2020-12-27 12:42:23', 0),
(46, 1, NULL, NULL, '+6287782757589', 'abcd', NULL, NULL, 'PQ3A.190605.003', '2020-11-30 14:24:08', 0),
(442, 6, NULL, NULL, '+6289664001634', 'eegq', NULL, NULL, 'NJH47F', '2021-01-01 16:42:45', 0),
(395, 11, NULL, NULL, '+6282132445667', 'uqgd', NULL, NULL, 'PKQ1.190319.001', '2020-12-21 12:04:03', 0),
(67, 1, NULL, NULL, '+6281387951341', 'rizal', NULL, NULL, 'NJH47B', '2021-01-03 23:41:37', 0),
(68, 1, NULL, NULL, '+628999531995', 'amaris', NULL, NULL, 'PPR1.180610.011', '2020-12-13 22:49:12', 0),
(69, 1, NULL, NULL, '+6281211339297', 'iway', NULL, NULL, 'N2G47H', '2020-12-31 22:59:09', 0),
(71, 1, NULL, NULL, '+6281298489066', 'arief', NULL, NULL, 'NRD90M', '2020-12-10 23:03:18', 0),
(308, 6, NULL, NULL, '+6287775801106', 'pale', NULL, NULL, 'PKQ1.190616.001', '2020-12-25 20:21:27', 0),
(307, 6, NULL, NULL, '+6282257115775', 'prnw', NULL, NULL, 'N2G47H', '2020-12-25 18:31:41', 0),
(74, 1, NULL, NULL, '+6285156342141', 'ubay', NULL, NULL, 'PI', '2021-01-31 22:55:39', 0),
(281, 1, NULL, NULL, '+6282112548530', 'ivan', NULL, NULL, 'MMB29M', '2020-12-20 15:40:16', 0),
(76, 1, NULL, NULL, '+6281806933013', 'gigin', NULL, NULL, 'M1AJQ', '2020-12-19 23:01:15', 0),
(77, 7, NULL, NULL, '+6285772172361', 'robi27', NULL, NULL, 'NJH47D', '2021-01-09 11:00:35', 0),
(78, 1, NULL, NULL, '+6281297099262', 'didi', NULL, NULL, 'MMB29T', '2021-01-04 23:28:52', 0),
(280, 6, NULL, NULL, '+6281315629645', 'iopj', NULL, NULL, 'MMB29T', '2020-12-20 15:37:38', 0),
(127, 6, NULL, NULL, '+6282114570456', 'kjhq', NULL, NULL, 'NRD90M', '2020-12-21 15:09:12', 0),
(82, 1, NULL, NULL, '+6281213990038', 'cocol', NULL, NULL, 'N2G47H', '2020-12-15 23:37:28', 0),
(253, 6, NULL, NULL, '+6281383507467', 'prgl', NULL, NULL, 'NJH47F', '2020-12-12 17:47:14', 0),
(85, 11, NULL, NULL, '+6282134094028', 'nana', NULL, NULL, 'QKQ1.191002.002', '2020-12-24 23:51:48', 0),
(394, 9, NULL, NULL, '+6282216240207', 'awan', NULL, NULL, 'NJH47F', '2020-12-22 10:36:24', 0),
(404, 6, NULL, NULL, '+6281280178919', 'vdjj', NULL, NULL, 'N2G47H', '2020-12-24 14:30:20', 0),
(440, 1, NULL, NULL, '+6285700033656', 'vtts', NULL, NULL, 'WW_Phone-201905061022', '2021-01-02 00:57:05', 0),
(306, 6, NULL, NULL, '+6281398693781', 'khdb', NULL, NULL, 'N2G47H', '2020-12-25 18:28:42', 0),
(92, 1, NULL, NULL, '+6288210505229', 'wbpi', NULL, NULL, 'NJH47F', '2021-01-05 17:48:10', 0),
(441, 1, NULL, NULL, '+6287889810721', 'zxnw', NULL, NULL, 'N2G47H', '2021-01-01 01:35:27', 0),
(337, 6, NULL, NULL, '+6282132900605', 'bmfc', NULL, NULL, 'NMF26X', '2020-12-02 15:26:48', 0),
(403, 6, NULL, NULL, '+6281218689544', 'mdco', NULL, NULL, 'PKQ1.180904.001', '2020-12-23 23:02:25', 0),
(279, 6, NULL, NULL, '+6281288270435', 'oskj', NULL, NULL, 'N2G47H', '2020-12-20 13:11:37', 0),
(108, 6, NULL, NULL, '+62881024942061', 'obeh', NULL, NULL, 'NMF26X', '2020-12-14 11:56:04', 0),
(110, 6, NULL, NULL, '+6287761336875', 'ykik', NULL, NULL, 'MMB29M', '2020-12-14 14:45:22', 0),
(451, 9, NULL, NULL, '+6282221965632', 'pngr', NULL, NULL, 'NMF26X', '2021-01-02 22:54:58', 0),
(343, 7, NULL, NULL, '+6281282450381', 'abdul23', NULL, NULL, 'MMB29M', '2021-01-04 01:00:47', 0),
(446, 6, NULL, NULL, '+6282110127947', 'mcct', NULL, NULL, 'NJH47B', '2021-01-02 12:08:52', 0),
(327, 8, NULL, NULL, '+6281808728889', 'didi2', NULL, NULL, 'NJH47F', '2020-12-30 14:23:39', 0),
(439, 1, NULL, NULL, '+6281385466207', 'aamf', NULL, NULL, 'NJH47F', '2021-01-06 00:16:57', 0),
(326, 8, NULL, NULL, '+6281291685579', 'didi', NULL, NULL, 'MMB29M', '2020-12-30 14:03:07', 0),
(437, 1, NULL, NULL, '+6289523338269', 'vwhx', NULL, NULL, 'PKQ1', '2020-12-30 19:35:15', 0),
(309, 8, NULL, NULL, '+6282210108310', 'arif2', NULL, NULL, 'QP1A.190711.020', '2020-12-25 21:23:19', 0),
(123, 1, NULL, NULL, '+6288112233', 'alizon', NULL, NULL, 'N2G47H', '2020-12-31 04:06:16', 0),
(124, 1, NULL, NULL, '+6282173012291', 'andri', NULL, NULL, 'MMB29K', '2020-12-20 08:23:14', 0),
(322, 8, NULL, NULL, '+6281382918468', 'pakde', NULL, NULL, 'N2G47H', '2020-12-29 19:44:32', 0),
(323, 6, NULL, NULL, '+6282295095093', 'qzvi', NULL, NULL, 'NJH47F', '2020-12-29 23:12:07', 0),
(324, 6, NULL, NULL, '+6281380286725', 'bzlp', NULL, NULL, 'mi-globe.com v10 V10.3.1.0.NAMCNXM', '2020-11-30 08:18:24', 0),
(438, 6, NULL, NULL, '+6281282331284', 'ecri', NULL, NULL, 'N2G47H', '2020-12-30 22:40:30', 0),
(136, 6, NULL, NULL, '+628128130479', 'hwth', NULL, NULL, 'PKQ1.190616.001', '2020-12-22 22:01:37', 0),
(325, 1, NULL, NULL, '+6281343567112', '38club', NULL, NULL, 'PKQ1.190319.001', '2020-12-24 10:54:23', 0),
(320, 6, NULL, NULL, '+6281288646590', 'yeip', NULL, NULL, 'NJH47F', '2020-12-28 21:33:38', 0),
(142, 1, NULL, NULL, '+6285281586331', 'iman', NULL, NULL, 'PPR1.180610.011', '2020-12-24 11:12:19', 0),
(391, 9, NULL, NULL, '+62881024587583', 'dika', NULL, NULL, 'NJH47F', '2020-12-19 19:18:17', 0),
(390, 1, NULL, NULL, '+628888144834', 'fahmi', NULL, NULL, 'N2G47H', '2020-12-18 17:47:17', 0),
(386, 6, NULL, NULL, '+6281299356136', 'zwob', NULL, NULL, 'N2G47H', '2020-12-17 21:27:52', 0),
(448, 6, NULL, NULL, '+6281278749913', 'sqfr', NULL, NULL, 'NJH47F', '2021-01-02 21:29:15', 0),
(149, 1, NULL, NULL, '+6281386155846', 'dede', NULL, NULL, 'N2G47H', '2020-12-25 11:11:52', 0),
(385, 6, NULL, NULL, '+6281952771550', 'zknh', NULL, NULL, 'PPR1.180610.011', '2020-12-17 18:54:19', 0),
(151, 6, NULL, NULL, '+6281285236918', 'joyd', NULL, NULL, 'NRD90M', '2020-12-25 15:08:40', 0),
(152, 6, NULL, NULL, '+6281231314420', 'mvkm', NULL, NULL, 'PKQ1.180904.001', '2020-12-25 18:22:27', 0),
(296, 6, NULL, NULL, '+6285799045758', 'brian', NULL, NULL, 'QD4A.200905.003', '2020-12-23 01:02:29', 0),
(362, 9, NULL, NULL, '+6281808382015', 'joko', NULL, NULL, 'PPR1.180610.011', '2020-12-14 15:31:16', 0),
(363, 6, NULL, NULL, '+628979283201', 'zxqq', NULL, NULL, 'QKQ1.200114.002', '2020-12-13 16:47:35', 0),
(336, 1, NULL, NULL, '+6282110127947', 'aris', NULL, NULL, 'NJH47B', '2020-12-02 12:59:02', 0),
(335, 1, NULL, NULL, '+6282230328441', 'pakl', NULL, NULL, 'PQ3A.190801.002', '2020-12-02 12:28:09', 0),
(384, 1, NULL, NULL, '+628123456789', 'andro', NULL, NULL, 'RSR1.201013.001', '2020-12-31 18:11:04', 0),
(334, 1, NULL, NULL, '+6281318319096', 'wlel', NULL, NULL, 'N2G47H', '2020-12-01 23:19:38', 0),
(383, 6, NULL, NULL, '+6281344599029', 'ffem', NULL, NULL, 'NRD90M', '2020-12-17 16:23:23', 0),
(164, 1, NULL, NULL, '+6281806933013', 'gigin', NULL, NULL, 'M1AJQ', '2020-12-19 12:01:42', 0),
(382, 8, NULL, NULL, '+6283120821756', 'rizal', NULL, NULL, 'N2G47H', '2020-12-17 14:27:29', 0),
(392, 9, NULL, NULL, '+6281218249634', 'dokie', NULL, NULL, 'PPR1.180610.011', '2020-12-20 22:06:14', 0),
(379, 11, NULL, NULL, '+6282132456888', 'ezol', NULL, NULL, 'PKQ1.190319.001', '2020-12-17 10:40:24', 0),
(167, 1, NULL, NULL, '+6281282301414', 'aromadi', NULL, NULL, 'PI', '2020-12-28 22:07:05', 0),
(168, 6, NULL, NULL, '+6281297202027', 'fxza', NULL, NULL, 'PKQ1.181021.001', '2020-12-28 22:56:03', 0),
(381, 6, NULL, NULL, '+6281393685225', 'ragx', NULL, NULL, 'NRD90M', '2020-12-17 14:25:14', 0),
(333, 6, NULL, NULL, '+6282292388665', 'adrq', NULL, NULL, 'QQ3A.200905.001', '2020-12-01 16:49:55', 0),
(339, 8, NULL, NULL, '+6281210284309', 'riandy', NULL, NULL, 'NRD90M', '2021-01-02 19:40:06', 0),
(361, 8, NULL, NULL, '+6281291863435', 'suraji', NULL, NULL, 'NJH47F', '2020-12-12 05:59:52', 0),
(338, 8, NULL, NULL, '+6281295297267', '211090', NULL, NULL, 'OPM1.171019.026', '2021-01-02 17:46:06', 0),
(175, 6, NULL, NULL, '+6285211484073', 'kwrr', NULL, NULL, 'OPM2.171026.006.H1', '2020-12-30 09:59:44', 0),
(344, 6, NULL, NULL, '+6281380791010', 'fmzi', NULL, NULL, 'N2G47H', '2021-01-04 20:44:26', 0),
(345, 1, NULL, NULL, '+6281384409088', 'iwan', NULL, NULL, 'N2G47H', '2020-12-05 13:22:08', 0),
(346, 6, NULL, NULL, '+6281803289595', 'sdzf', NULL, NULL, 'NRD90M', '2021-01-05 23:30:51', 0),
(179, 6, NULL, NULL, '+62811605774', 'qzgc', NULL, NULL, 'PKQ1.190616.001', '2020-11-30 15:11:29', 0),
(400, 6, NULL, NULL, '+6282143191723', 'cwes', NULL, NULL, 'NRD90M', '2020-12-22 14:42:27', 0),
(405, 1, NULL, NULL, '+6282334986373', 'zypo', NULL, NULL, 'QQ3A.200905.001', '2020-12-24 18:53:23', 0),
(183, 6, NULL, NULL, '+6281386966817', 'abbu', NULL, NULL, 'NRD90M', '2020-11-30 21:28:21', 0),
(342, 1, NULL, NULL, '+6289510880729', 'ramdani', NULL, NULL, 'MMB29M', '2021-01-04 13:02:36', 0),
(185, 6, NULL, NULL, '+6285959863747', 'yjgp', NULL, NULL, 'M1AJQ', '2021-01-01 12:07:46', 0),
(380, 11, NULL, NULL, '+6281321768334', 'uasb', NULL, NULL, 'QKQ1.191215.002', '2020-12-17 10:53:36', 0),
(187, 1, NULL, NULL, '+6281210073230', 'iymh', NULL, NULL, 'NRD90M', '2021-01-10 15:22:59', 0),
(364, 1, NULL, NULL, '+628999995602', 'clpc', NULL, NULL, 'PKQ1.190101.001', '2020-12-13 20:37:03', 0),
(387, 8, NULL, NULL, '+6281383060536', 'pakde2', NULL, NULL, 'N2G47H', '2020-12-17 11:13:48', 0),
(352, 6, NULL, NULL, '+6281389018823', 'azjb', NULL, NULL, 'QQ2A.200305.003', '2020-12-08 19:20:15', 0),
(371, 6, NULL, NULL, '+6281283426211', 'tudk', NULL, NULL, 'NRD90M', '2020-12-15 19:26:49', 0),
(196, 1, NULL, NULL, '+6289603217211', 'rahman', NULL, NULL, 'QP1A.190711.020', '2021-01-08 13:14:35', 0),
(350, 6, NULL, NULL, '+6282249008868', 'cqye', NULL, NULL, 'MMB29M', '2020-12-07 22:51:53', 0),
(351, 6, NULL, NULL, '+6281213005086', 'leem', NULL, NULL, 'NJH47F', '2021-01-07 23:11:00', 0),
(376, 6, NULL, NULL, '+6281282331284', 'yndw', NULL, NULL, 'PKQ1.181021.001', '2020-12-16 17:49:36', 0),
(201, 1, NULL, NULL, '+6285777888132', 'bwyw', NULL, NULL, 'N2G47H', '2021-01-04 14:07:58', 0),
(348, 6, NULL, NULL, '+6289648004800', 'wvve', NULL, NULL, 'NJH47F', '2020-12-07 00:58:02', 0),
(205, 1, NULL, NULL, '+6285973970055', 'sarman', NULL, NULL, 'MMB29T', '2021-01-05 11:54:19', 0),
(206, 7, NULL, NULL, '+6282113190223', 'rojo', NULL, NULL, 'N2G47H', '2021-01-05 13:00:28', 0),
(208, 6, NULL, NULL, '+6285881310883', 'zicn', NULL, NULL, 'PKQ1.190118.001', '2020-12-06 17:52:18', 0),
(347, 6, NULL, NULL, '+6281294358783', 'ikjd', NULL, NULL, 'PPR1.180610.011', '2020-12-06 17:27:25', 0),
(210, 1, NULL, NULL, '+6282178367778', 'agus', NULL, NULL, 'OPR1.170623.032', '2021-01-05 21:40:44', 0),
(360, 9, NULL, NULL, '+6281318077560', 'masbay', NULL, NULL, 'MMB29M', '2020-12-12 12:24:28', 0),
(358, 6, NULL, NULL, '+6281211722127', 'swqg', NULL, NULL, 'NJH47F', '2020-12-10 17:02:18', 0),
(359, 11, NULL, NULL, '+6283134095025', 'suaa', NULL, NULL, 'QQ3A.200705.002', '2021-01-11 11:13:15', 0),
(355, 7, NULL, NULL, '+6281298488131', 'giant2020', NULL, NULL, 'N2G47H', '2021-01-09 14:00:53', 0),
(357, 11, NULL, NULL, '+6282133094022', 'wste', NULL, NULL, 'PKQ1.190319.001', '2021-01-10 14:51:58', 0),
(356, 1, NULL, NULL, '+6287883605764', 'abcd', NULL, NULL, 'PPR1.180610.011', '2021-01-31 14:10:29', 0),
(375, 6, NULL, NULL, '+6281398066355', 'uftm', NULL, NULL, 'NJH47F', '2020-12-16 17:18:58', 0),
(221, 1, NULL, NULL, '+62895331275355', 'rego', NULL, NULL, 'MMB29M', '2021-01-03 12:47:33', 0),
(222, 7, NULL, NULL, '+6283181328585', 'hendrik7', NULL, NULL, 'NJH47F', '2021-01-07 13:05:13', 0),
(353, 6, NULL, NULL, '+6281296087694', 'fpka', NULL, NULL, 'N2G47H', '2020-12-09 01:51:09', 0),
(515, 1, NULL, NULL, '+62164483984', 'gasq', NULL, NULL, 'PKQ1.190319.001', '2021-01-08 09:05:21', 0),
(225, 6, NULL, NULL, '+6281317163787', 'hbla', NULL, NULL, 'PKQ1.181021.001', '2021-01-07 20:09:13', 0),
(393, 9, NULL, NULL, '+6281218366583', 'agung', NULL, NULL, 'N2G47H', '2020-12-21 14:26:01', 0),
(369, 6, NULL, NULL, '+6281224020598', 'pqqt', NULL, NULL, 'QP1A.190711.020', '2020-12-15 15:36:01', 0),
(368, 8, NULL, NULL, '+6281285786445', 'dayat', NULL, NULL, 'N2G47H', '2020-12-14 15:09:57', 0),
(229, 6, NULL, NULL, '+6281213468046', 'whcu', NULL, NULL, 'PPR1.180610.011', '2021-01-08 15:35:40', 0),
(366, 6, NULL, NULL, '+6282114725570', 'uncr', NULL, NULL, 'unknown', '2020-12-14 09:39:49', 0),
(232, 11, NULL, NULL, '+62893644476', 'giwq', NULL, NULL, 'PKQ1.190319.001', '2020-12-09 09:58:28', 0),
(436, 1, NULL, NULL, '+6281311222211', 'core', NULL, NULL, 'PKQ1.180904.001', '2020-12-31 18:16:22', 0),
(374, 11, NULL, NULL, '+6281342760088', 'fnkx', NULL, NULL, 'PKQ1.190319.001', '2020-12-16 11:20:58', 0),
(235, 1, NULL, NULL, '+62123465185', 'test', NULL, NULL, 'PKQ1.190319.001', '2021-01-10 23:08:32', 0),
(432, 1, NULL, NULL, '+6289665310312', 'abvw', NULL, NULL, 'NJH47F', '2020-12-29 21:38:51', 0),
(431, 1, NULL, NULL, '+6281212146205', 'reza', NULL, NULL, 'NJH47F', '2020-12-29 17:51:11', 0),
(429, 11, NULL, NULL, '+62811235689598', 'beza', NULL, NULL, 'PKQ1.180917.001', '2020-12-29 15:27:26', 0),
(240, 6, NULL, NULL, '+6285258660155', 'nxbl', NULL, NULL, 'N2G47H', '2020-12-10 17:47:58', 0),
(349, 6, NULL, NULL, '+628122822233', 'kgzb', NULL, NULL, 'NJH47F', '2020-12-07 14:05:26', 0),
(243, 6, NULL, NULL, '+6287879451120', 'hnue', NULL, NULL, 'PI', '2020-12-10 22:34:55', 0),
(427, 9, NULL, NULL, '+6281292926669', 'irwan', NULL, NULL, 'Asarre Rom v2', '2020-12-29 18:26:26', 0),
(428, 6, NULL, NULL, '+6285156175495', 'jcgi', NULL, NULL, 'OPM1.171019.019', '2020-12-28 21:29:39', 0),
(247, 6, NULL, NULL, '+6281806777133', 'ehbq', NULL, NULL, 'PKQ1.190319.001', '2020-12-11 15:46:47', 0),
(389, 6, NULL, NULL, '+6288210237332', 'aipe', NULL, NULL, 'OPM1.171019.011', '2020-12-18 14:18:05', 0),
(365, 8, NULL, NULL, '+6281314561410', 'arif', NULL, NULL, 'NRD90M', '2020-12-14 06:42:25', 0),
(258, 1, NULL, NULL, '+62813344', 'test', NULL, NULL, 'PQ3B.190801.002', '2020-10-14 20:21:42', 0),
(435, 1, NULL, NULL, '+6282113969578', 'noab', NULL, NULL, 'NJH47F', '2020-12-30 16:57:49', 0),
(434, 1, NULL, NULL, '+62124066323', 'uqcn', NULL, NULL, 'PPR1.180610.011', '2020-12-30 14:56:37', 0),
(261, 1, NULL, NULL, '+6285211745105', 'alam', NULL, NULL, 'MMB29T', '2021-01-14 13:05:24', 0),
(262, 6, NULL, NULL, '+6281222288752', 'kbyj', NULL, NULL, 'N2G47H', '2020-12-14 14:48:10', 0),
(263, 6, NULL, NULL, '+6289505222416', 'ofwb', NULL, NULL, 'MMB29K', '2020-12-14 14:49:30', 0),
(433, 1, NULL, NULL, '+6281224457763', 'sxud', NULL, NULL, 'NJH47F', '2020-12-30 02:59:23', 0),
(265, 1, NULL, NULL, '+6281382105652', 'gvxg', NULL, NULL, 'N2G47H', '2020-12-15 14:24:12', 0),
(373, 8, NULL, NULL, '+6281383796081', 'bayu', NULL, NULL, 'M1AJQ', '2020-12-15 22:55:25', 0),
(430, 11, NULL, NULL, '+62823568946', 'zaza', NULL, NULL, 'NMF26X', '2020-12-29 15:39:00', 0),
(367, 6, NULL, NULL, '+6285695266266', 'svcz', NULL, NULL, 'OPM6.171019.030.H1', '2020-12-14 14:30:07', 0),
(372, 8, NULL, NULL, '+626282113058543', 'misdar', NULL, NULL, 'MMB29M', '2020-12-15 20:34:45', 0),
(388, 1, NULL, NULL, '+62164511883', 'cscm', NULL, NULL, 'PKQ1.190319.001', '2020-12-18 12:08:04', 0),
(321, 1, NULL, NULL, '+6285156119228', 'ali', NULL, NULL, 'N2G47H', '2020-12-31 19:02:33', 0),
(402, 1, NULL, NULL, '+6281285243954', 'wvkt', NULL, NULL, 'MMB29T', '2020-12-23 22:44:23', 0),
(426, 1, NULL, NULL, '+6287737133456', '38elubb', NULL, NULL, 'PKQ1.190319.001', '2020-12-27 10:54:58', 0),
(401, 1, NULL, NULL, '+6289614906217', 'azis', NULL, NULL, 'QQ3A.200805.001', '2020-12-23 21:17:14', 0),
(276, 6, NULL, NULL, '+6281289753860', 'ifhq', NULL, NULL, 'OPM8.190205.001', '2020-12-19 01:19:16', 0),
(284, 6, NULL, NULL, '+6282130118186', 'gomk', NULL, NULL, 'QP1A.190711.020', '2020-12-21 13:09:59', 0),
(285, 1, NULL, NULL, '+6285282540155', 'zbdd', NULL, NULL, 'OPM1.171019.026', '2020-12-21 14:21:44', 0),
(286, 6, NULL, NULL, '+6281932368020', 'qrig', NULL, NULL, 'NJH47F', '2020-12-21 15:52:47', 0),
(425, 1, NULL, NULL, '+626287737133456', '38club', NULL, NULL, 'PKQ1.190319.001', '2020-12-27 09:48:29', 0),
(288, 1, NULL, NULL, '+6281291491626', 'zacky', NULL, NULL, 'O11019', '2020-12-21 16:49:22', 0),
(289, 6, NULL, NULL, '+6281288366058', 'ykof', NULL, NULL, 'N2G47H', '2020-12-22 21:26:17', 0),
(424, 6, NULL, NULL, '+62895332845864', 'ygcu', NULL, NULL, 'OPM6.171019.030.B1', '2020-12-27 13:32:54', 0),
(291, 1, NULL, NULL, '+6281398337781', 'asep', NULL, NULL, 'NJH47F', '2020-12-22 14:07:55', 0),
(292, 1, NULL, NULL, '+6281294814460', 'fani', NULL, NULL, 'MMB29T', '2020-12-22 14:37:04', 0),
(293, 11, NULL, NULL, '+6217737771456', 'mzky', NULL, NULL, 'PKQ1.190319.001', '2020-12-22 16:55:31', 0),
(294, 6, NULL, NULL, '+6281388378213', 'abwn', NULL, NULL, 'QP1A.190711.020', '2020-12-22 21:04:44', 0),
(297, 6, NULL, NULL, '+628155712113', 'cbbi', NULL, NULL, 'PQ3B.190801.002', '2020-12-23 17:08:12', 0),
(299, 6, NULL, NULL, '+6281281082132', 'ziwq', NULL, NULL, 'OPM1.171019.026', '2020-12-23 20:10:19', 0),
(301, 6, NULL, NULL, '+6281338763392', 'egss', NULL, NULL, 'MMB29M', '2020-12-23 22:41:43', 0),
(447, 10, NULL, NULL, '+6287883908685', 'jancok22', NULL, NULL, 'NJH47F', '2021-01-02 16:23:53', 0),
(303, 6, NULL, NULL, '+6281293879599', 'prse', NULL, NULL, 'N2G48H', '2020-12-24 10:39:29', 0),
(445, 10, NULL, NULL, '+6281907129780', 'fahry', NULL, NULL, 'MMB29M', '2021-01-02 00:03:03', 0),
(311, 6, NULL, NULL, '+6281283568989', 'wrga', NULL, NULL, 'NJH47F', '2020-12-26 12:47:57', 0),
(312, 6, NULL, NULL, '+6283819706221', 'tlep', NULL, NULL, 'NJH47F', '2020-12-26 13:43:12', 0),
(315, 8, NULL, NULL, '+6289603505445', 'anjar', NULL, NULL, 'N2G47H', '2020-12-27 14:52:02', 0),
(317, 8, NULL, NULL, '+6281298138456', 'frans', NULL, NULL, 'PQ3B.190801.002', '2020-12-27 20:27:17', 0),
(340, 6, NULL, NULL, '+6285882347423', 'mdxi', NULL, NULL, 'N2G47H', '2021-01-03 17:40:21', 0),
(318, 8, NULL, NULL, '+6281291461953', 'sulaiman', NULL, NULL, 'N2G47H', '2020-12-28 16:12:07', 0),
(450, 10, NULL, NULL, '+6287803567782', 'ali', NULL, NULL, 'PPR1.180610.011', '2021-01-02 22:45:57', 0),
(329, 8, NULL, NULL, '+6287712184774', 'fatimah', NULL, NULL, 'M1AJQ', '2020-12-30 09:42:39', 0),
(330, 6, NULL, NULL, '+6285217203559', 'wijf', NULL, NULL, 'MMB29M', '2020-11-30 11:18:14', 0),
(331, 6, NULL, NULL, '+628128149788', 'aiqw', NULL, NULL, 'MMB29T', '2020-11-30 15:31:19', 0),
(332, 6, NULL, NULL, '+6281288838252', 'btvv', NULL, NULL, 'N2G47H', '2020-11-30 16:03:27', 0),
(396, 11, NULL, NULL, '+6282135676880', 'sgkv', NULL, NULL, 'MMB29K', '2020-12-21 12:07:28', 0),
(398, 6, NULL, NULL, '+6281318200472', 'yedt', NULL, NULL, 'QD4A.200905.003', '2020-12-21 17:43:53', 0),
(399, 6, NULL, NULL, '+6281287068099', 'wwow', NULL, NULL, 'MRA58K', '2020-12-21 18:45:46', 0),
(406, 9, NULL, NULL, '+6289631800403', 'danis', NULL, NULL, 'QQ3A.200905.001', '2020-12-25 19:02:22', 0),
(407, 1, NULL, NULL, '+6281332744589', 'fajar', NULL, NULL, 'QQ3A.200905.001', '2021-01-31 19:03:00', 0),
(408, 6, NULL, NULL, '+628990005514', 'araw', NULL, NULL, 'N2G47H', '2021-01-24 20:37:39', 0),
(409, 1, NULL, NULL, '+6281286207731', 'apeng', NULL, NULL, 'QP1A.190711.020', '2020-12-03 12:18:48', 0),
(410, 1, NULL, NULL, '+6281297971094', 'solihin', NULL, NULL, 'NRD90M', '2020-12-25 12:45:39', 0),
(411, 9, NULL, NULL, '+6285924108935', 'arif', NULL, NULL, 'PPR1.180610.011', '2020-12-26 17:52:29', 0),
(412, 1, NULL, NULL, '+621110305185', 'test', NULL, NULL, 'PKQ1.190319.001', '2020-12-25 17:52:54', 0),
(413, 1, NULL, NULL, '+62895349894702', 'dada', NULL, NULL, 'QD4A.200905.003', '2020-12-31 21:50:01', 0),
(414, 6, NULL, NULL, '+6281333906107', 'ujfg', NULL, NULL, 'MMB29M', '2020-12-25 21:51:37', 0),
(415, 1, NULL, NULL, '+6289638194260', 'drtk', NULL, NULL, 'MMB29T', '2020-12-25 22:24:21', 0),
(416, 1, NULL, NULL, '+6281317861904', 'irfan', NULL, NULL, 'QQ3A.200805.001', '2020-12-15 22:57:32', 0),
(417, 6, NULL, NULL, '+6281319864795', 'ydub', NULL, NULL, 'NRD90M', '2020-12-26 00:16:20', 0),
(418, 1, NULL, NULL, '+62817428899', 'andre', NULL, NULL, 'PPR1.180610.011', '2020-12-24 14:14:52', 0),
(419, 1, NULL, NULL, '+6281287970429', 'gzdg', NULL, NULL, 'NRD90M', '2020-12-26 18:27:04', 0),
(420, 6, NULL, NULL, '+6281296325631', 'grcf', NULL, NULL, 'PKQ1.180904.001', '2020-12-26 18:39:46', 0),
(421, 6, NULL, NULL, '+6281318330251', 'fxks', NULL, NULL, 'NJH47F', '2020-12-26 22:36:54', 0),
(422, 1, NULL, NULL, '+6281285243954', 'rian', NULL, NULL, 'PQ3A.190801.002', '2020-12-27 01:56:51', 0),
(452, 1, NULL, NULL, '+6282246292897', 'bayu', NULL, NULL, 'PQ3A.190801.002', '2021-01-03 10:36:06', 0),
(453, 6, NULL, NULL, '+6289685016757', 'qbtw', NULL, NULL, 'PKQ1.180917.001', '2021-01-03 21:15:08', 0),
(454, 11, NULL, NULL, '+62886598356', 'xxcc', NULL, NULL, 'PPR1.180610.011', '2021-01-04 15:33:04', 0),
(455, 1, NULL, NULL, '+6289527517141', 'fqym', NULL, NULL, 'OPM8.190205.001', '2021-01-04 16:45:35', 0),
(456, 1, NULL, NULL, '+6285280073579', 'hpdj', NULL, NULL, 'N2G47H', '2021-01-04 16:55:57', 0),
(457, 1, NULL, NULL, '+6281296348321', 'topw', NULL, NULL, 'QQ3A.200905.001', '2020-12-23 22:52:26', 0),
(458, 1, NULL, NULL, '+6285887506494', 'xpjz', NULL, NULL, 'M1AJQ', '2021-01-05 10:21:24', 0),
(463, 12, NULL, NULL, '+6281392524344', 'pchy', NULL, NULL, 'NRD90M', '2020-12-07 23:59:39', 0),
(460, 12, NULL, NULL, '+6285884951735', 'xamd', NULL, NULL, 'QQ3A.200805.001', '2020-12-07 23:59:49', 0),
(461, 12, NULL, NULL, '+62882009617890', 'bxdu', NULL, NULL, 'N2G47H', '2020-12-07 23:59:03', 0),
(462, 12, NULL, NULL, '+6285890388699', 'xozi', NULL, NULL, 'QP1A.190711.020', '2020-12-07 23:59:13', 0),
(464, 9, NULL, NULL, '+62895396896590', 'daus', NULL, NULL, 'N2G47H', '2021-01-05 18:53:55', 0),
(513, 6, NULL, NULL, '+628970856995', 'wsii', NULL, NULL, 'NJH47F', '2021-01-07 23:47:12', 0),
(466, 12, NULL, NULL, '+6283126247221', 'hgcv', NULL, NULL, 'PQ3A.190801.002', '2020-12-07 23:59:36', 0),
(512, 6, NULL, NULL, '+628987828284', 'nlrz', NULL, NULL, 'PQ3A.190801.002', '2021-01-07 23:40:50', 0),
(468, 12, NULL, NULL, '+6285237856770', 'zykv', NULL, NULL, 'NRD90M', '2020-12-07 23:59:00', 0),
(469, 12, NULL, NULL, '+62895374922853', 'rgxn', NULL, NULL, 'NJH47F', '2020-12-07 23:59:21', 0),
(470, 12, NULL, NULL, '+62895622346416', 'ykgx', NULL, NULL, 'MOB31K', '2020-12-07 23:59:42', 0),
(471, 12, NULL, NULL, '+6285716516948', 'lkgu', NULL, NULL, 'PKQ1.190319.001', '2020-12-07 23:59:44', 0),
(472, 12, NULL, NULL, '+62895397511011', 'wxpk', NULL, NULL, 'N2G47H', '2020-12-07 23:59:04', 0),
(473, 12, NULL, NULL, '+6281585440430', 'zcza', NULL, NULL, 'PKQ1.190319.001', '2020-12-07 23:59:32', 0),
(474, 12, NULL, NULL, '+6281285304624', 'gxgn', NULL, NULL, 'RD1A.201105.003.C1', '2020-12-07 23:59:08', 0),
(475, 12, NULL, NULL, '+6285714678372', 'inyx', NULL, NULL, 'QQ3A.200805.001', '2020-12-07 23:59:44', 0),
(476, 12, NULL, NULL, '+6281218191918', 'jezg', NULL, NULL, 'PKQ1.180904.001', '2020-12-07 23:59:39', 0),
(477, 12, NULL, NULL, '+6281295522519', 'ogzs', NULL, NULL, 'NJH47F', '2020-12-07 23:59:14', 0),
(478, 12, NULL, NULL, '+6281287878887', 'ysfx', NULL, NULL, 'N2G47H', '2020-12-07 23:59:03', 0),
(479, 12, NULL, NULL, '+6282135585084', 'zyxj', NULL, NULL, 'QQ2A.200305.003', '2020-12-07 23:59:51', 0),
(480, 12, NULL, NULL, '+6285603771774', 'ccxo', NULL, NULL, 'N2G47H', '2020-12-07 23:59:18', 0),
(481, 1, NULL, NULL, '+6282116892356', 'qlze', NULL, NULL, 'MMB29T', '2021-01-06 17:21:25', 0),
(482, 1, NULL, NULL, '+6282129001901', 'wwfn', NULL, NULL, 'NJH47F', '2021-01-06 17:22:28', 0),
(483, 1, NULL, NULL, '+6281219365835', 'tboc', NULL, NULL, 'NJH47F', '2021-01-07 20:37:34', 0),
(484, 1, NULL, NULL, '+6289525770514', 'scqv', NULL, NULL, 'PPR1.180610.011', '2020-12-20 20:44:39', 0),
(485, 1, NULL, NULL, '+6281283097263', 'nzbp', NULL, NULL, 'MRA58K', '2020-12-25 20:49:27', 0),
(486, 1, NULL, NULL, '+6289614382033', 'bdan', NULL, NULL, 'N2G47H', '2020-12-22 20:52:47', 0),
(487, 12, NULL, NULL, '+628563218011', 'zuxs', NULL, NULL, 'OPR1.170623.032', '2020-12-07 23:59:06', 0),
(488, 1, NULL, NULL, '+6288290086975', 'odgj', NULL, NULL, 'NJH47F', '2021-01-06 22:07:31', 0),
(489, 9, NULL, NULL, '+6281284941914', 'sqpa', NULL, NULL, 'MMB29P', '2021-01-06 22:16:18', 0),
(490, 1, NULL, NULL, '+6281381865604', 'rntg', NULL, NULL, 'MMB29T', '2020-12-14 22:39:27', 0),
(491, 1, NULL, NULL, '+6282124076860', 'qahw', NULL, NULL, 'NRD90M', '2020-12-22 22:40:51', 0),
(492, 12, NULL, NULL, '+6282242285959', 'jssv', NULL, NULL, 'NRD90M', '2020-12-07 23:59:27', 0),
(493, 12, NULL, NULL, '+6282324125867', 'frpx', NULL, NULL, 'PPR1.180610.011', '2020-12-07 23:59:49', 0),
(494, 6, NULL, NULL, '+6281218191918', 'cfbb', NULL, NULL, 'PKQ1.180904.001', '2021-01-06 23:15:02', 0),
(495, 12, NULL, NULL, '+6281387588985', 'etsj', NULL, NULL, 'OPM2.171026.006.C1', '2020-12-07 23:59:34', 0),
(496, 12, NULL, NULL, '+6281218938596', 'uixm', NULL, NULL, 'PQ3A.190801.002', '2020-12-07 23:59:49', 0),
(497, 6, NULL, NULL, '+62816250996', 'wsey', NULL, NULL, 'PKQ1.180904.001', '2021-01-07 00:40:25', 0),
(498, 12, NULL, NULL, '+6287760200033', 'ixvj', NULL, NULL, 'PKQ1.190118.001', '2020-12-07 23:59:45', 0),
(499, 12, NULL, NULL, '+6281246663252', 'lgwk', NULL, NULL, 'N2G47H', '2020-12-07 23:59:27', 0),
(500, 11, NULL, NULL, '+62816538956', 'dfre', NULL, NULL, 'QKQ1.191215.002', '2021-01-07 11:09:44', 0),
(501, 12, NULL, NULL, '+6281387588985', 'ewpp', NULL, NULL, 'OPM2.171026.006.C1', '2020-12-07 23:58:04', 0),
(502, 12, NULL, NULL, '+6281388032008', 'ogpy', NULL, NULL, 'PPR1.180610.011', '2020-12-07 23:59:20', 0),
(503, 12, NULL, NULL, '+6281398356464', 'phyv', NULL, NULL, 'QQ3A.200605.001', '2020-12-07 23:59:07', 0),
(511, 6, NULL, NULL, '+628122822233', 'ftgf', NULL, NULL, 'NJH47F', '2021-01-07 23:37:01', 0),
(506, 12, NULL, NULL, '+6285773660472', 'aomk', NULL, NULL, 'QP1A.190711.020', '2020-12-07 23:59:28', 0),
(507, 12, NULL, NULL, '+6281392700097', 'ipts', NULL, NULL, 'QKQ1.200114.002', '2020-12-07 23:59:59', 0),
(508, 12, NULL, NULL, '+6281281715063', 'fnim', NULL, NULL, 'QKQ1.190910.002', '2020-12-07 23:58:14', 0),
(509, 8, NULL, NULL, '+6285693743011', 'indri01', NULL, NULL, 'NJH47F', '2021-01-07 17:06:20', 0),
(510, 6, NULL, NULL, '+6282288750398', 'cwsn', NULL, NULL, 'QQ3A.200805.001', '2021-01-07 21:18:55', 0),
(530, 6, NULL, NULL, '+6281281715063', 'tqmr', NULL, NULL, 'QKQ1.190910.002', '2021-01-10 10:53:39', 0),
(516, 13, NULL, NULL, '+6281288497781', 'tsfq', NULL, NULL, 'N2G47H', '2021-01-06 09:20:17', 0),
(517, 13, NULL, NULL, '+6287883225193', 'exbs', NULL, NULL, 'PKQ1.181203.001', '2021-01-06 09:30:43', 0),
(518, 1, NULL, NULL, '+6281324977963', 'hugf', NULL, NULL, 'PPR1.180610.011', '2021-01-08 14:28:38', 0),
(519, 1, NULL, NULL, '+628997723109', 'dede', NULL, NULL, 'NRD90M', '2021-01-08 17:23:14', 0),
(520, 6, NULL, NULL, '+62895374922853', 'cptu', NULL, NULL, 'NJH47F', '2021-01-08 17:37:20', 0),
(521, 1, NULL, NULL, '+6281280996160', 'rqwm', NULL, NULL, 'QP1A.190711.020', '2020-12-15 18:32:54', 0),
(522, 6, NULL, NULL, '+6289670076727', 'oxev', NULL, NULL, 'M1AJQ', '2021-01-08 18:56:37', 0),
(523, 6, NULL, NULL, '+6281284942776', 'ewlg', NULL, NULL, 'PKQ1.181203.001', '2021-01-08 20:51:07', 0),
(524, 6, NULL, NULL, '+628818865437', 'jxmp', NULL, NULL, 'N2G47H', '2021-01-09 10:29:01', 0),
(525, 6, NULL, NULL, '+6282166294497', 'gerfil', NULL, NULL, 'PKQ1.180904.001', '2021-01-09 12:53:19', 0),
(526, 6, NULL, NULL, '+6287884120079', 'eyob', NULL, NULL, 'MMB29M', '2021-01-09 12:54:02', 0),
(527, 6, NULL, NULL, '+6281238916259', 'fdkp', NULL, NULL, 'PKQ1.190118.001', '2021-01-09 16:31:59', 0),
(528, 8, NULL, NULL, '+6281284504582', 'amir', NULL, NULL, 'OPR1.170623.032', '2021-01-08 20:40:18', 0),
(529, 6, NULL, NULL, '+6287879191912', 'xqzp', NULL, NULL, 'MRA58K', '2021-01-09 21:38:28', 0),
(532, 14, NULL, NULL, '+6285780302489', 'jebew', NULL, NULL, 'N2G47H', '2020-12-12 23:59:34', 0),
(533, 14, NULL, NULL, '+6285591242716', 'haikal', NULL, NULL, 'NRD90M', '2020-12-12 23:59:56', 0),
(534, 9, NULL, NULL, '+6282279778931', 'abcd', NULL, NULL, 'NJH47D', '2021-01-10 20:23:19', 0),
(535, 1, NULL, NULL, '+6281214043697', 'fdsw', NULL, NULL, 'MMB29T', '2021-01-10 21:55:41', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `superadmin`
--
ALTER TABLE `superadmin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `superadmin`
--
ALTER TABLE `superadmin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=536;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
