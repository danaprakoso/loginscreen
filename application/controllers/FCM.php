<?php

class FCM {

	public static function send_message($title, $body, $token, $data) {
		$url = 'https://fcm.googleapis.com/fcm/send';
	    $fields = array(
            'registration_ids' => array($token),
            'notification' => array(
            	'title' => $title,
            	'body' => $body
            )
    	);
    	if (sizeof($data) > 0) {
    		$fields['data'] = $data;
    	}
    	$fields = json_encode($fields);
    	$headers = array (
            'Authorization: key=' . "AAAAEOg4BeQ:APA91bEy6qwgED3vVFFjs544JLmN2ojxUASLsCvANZIbkJf7VDhhnKlaTT8-5QaqAHfr-A1vmnS_qriNqmwWFUjOf4GFBcay70KheGGRF-DDG6mVfo_YumNjYyTucoRDPG3QC-ACmKKu",
            'Content-Type: application/json'
    	);
    	$ch = curl_init ();
    	curl_setopt ( $ch, CURLOPT_URL, $url );
    	curl_setopt ( $ch, CURLOPT_POST, true );
    	curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
    	curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    	curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
	    $result = curl_exec ( $ch );
	    echo $result;
	    curl_close ( $ch );
	}
}
