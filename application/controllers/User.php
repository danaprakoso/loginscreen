<?php

include "FCM.php";
include "Util.php";

class User extends CI_Controller {
	
	private function generateRandomNumber($length) {
	    $result = '';
    	for($i = 0; $i < $length; $i++) {
    	    $result .= mt_rand(0, 9);
    	}
	    return $result;
	}

	public function login() {
		$phone = $this->input->post('phone');
		$password = $this->input->post('password');
		$date = $this->input->post('date');
		$androidID = $this->input->post('android_id');
		$users = $this->db->query("SELECT * FROM `user` WHERE `phone`='" . $phone . "' AND `password`='" . $password . "'")->result_array();
		if (sizeof($users) > 0) {
			$user = $users[0];
			/*if ($user['android_id'] == NULL || $user['android_id'] == null || $user['android_id'] == '') {
				$this->db->where('id', intval($user['id']));
				$this->db->update('user', array(
					'android_id' => $androidID
				));
				echo json_encode(array(
					'response_code' => 1,
					'user_id' => intval($user['id'])
				));
			} else {*/
				if ($user['android_id'] != $androidID) {
					echo json_encode(array(
						'response_code' => -1,
						'user_id' => intval($user['id']),
						'expiry' => $user['expiry']
					));
				} else {
					$expiry = $user['expiry'];
					if (strtotime($date) >= strtotime($expiry)) {
						echo json_encode(array(
							'response_code' => -3,
							'user_id' => intval($user['id']),
							'expiry' => $user['expiry']
						));
					} else {
						$suspended = intval($user['suspended']);
						if ($suspended == 1) {
							echo json_encode(array(
								'response_code' => -4,
								'user_id' => intval($user['id']),
								'expiry' => $user['expiry']
							));
						} else {
							echo json_encode(array(
								'response_code' => 1,
								'user_id' => intval($user['id']),
								'expiry' => $user['expiry']
							));
						}
					}
				}
			/*}*/
		} else {
			echo json_encode(array(
				'response_code' => -2
			));
		}
	}
	
	public function login_with_email() {
		$email = $this->input->post('email');
		$date = $this->input->post('date');
		$androidID = $this->input->post('android_id');
		$users = $this->db->query("SELECT * FROM `user` WHERE `email`='" . $email . "'")->result_array();
		if (sizeof($users) > 0) {
			$user = $users[0];
			/*if ($user['android_id'] == NULL || $user['android_id'] == null || $user['android_id'] == '') {
				$this->db->where('id', intval($user['id']));
				$this->db->update('user', array(
					'android_id' => $androidID
				));
				echo json_encode(array(
					'response_code' => 1,
					'user_id' => intval($user['id'])
				));
			} else {*/
				if ($user['android_id'] != $androidID) {
					$code = Util::generateRandomNumber(6);
					Util::send_email($user['email'], 'Kode verifikasi email Anda: ' . $code, 'Mohon masukkan 6-digit kode verifikasi berikut di kotak yang tersedia di aplikasi: <b>' . $code . '</b>');
					echo json_encode(array(
						'response_code' => -1,
						'user_id' => intval($user['id']),
						'expiry' => $user['expiry'],
						'code' => $code
					));
				} else {
					$expiry = $user['expiry'];
					if (strtotime($date) >= strtotime($expiry)) {
						echo json_encode(array(
							'response_code' => -3,
							'user_id' => intval($user['id']),
							'expiry' => $user['expiry']
						));
					} else {
						$suspended = intval($user['suspended']);
						if ($suspended == 1) {
							echo json_encode(array(
								'response_code' => -4,
								'user_id' => intval($user['id']),
								'expiry' => $user['expiry']
							));
						} else {
							echo json_encode(array(
								'response_code' => 1,
								'user_id' => intval($user['id']),
								'expiry' => $user['expiry']
							));
						}
					}
				}
			/*}*/
		} else {
			echo json_encode(array(
				'response_code' => -2
			));
		}
	}

	public function login_with_google() {
		$phone = $this->input->post('phone');
		$googleID = $this->input->post('google_id');
		$androidID = $this->input->post('android_id');
		$expiry = $this->input->post('expiry');
		$users = $this->db->query("SELECT * FROM `user` WHERE `google_id`='" . $googleID . "'")->result_array();
		if (sizeof($users) > 0) {
			$user = $users[0];
			$userID = intval($user['id']);
			$this->db->query("UPDATE `user` SET `android_id`='" . $androidID . "', `phone`='" . $phone . "', `expiry`='" . $expiry . "' WHERE `id`=" . $userID);
			echo json_encode(array(
				'response_code' => 1,
				'user_id' => $userID,
				'profile_completed' => intval($user['profile_completed']),
				'user' => $user
			));
		} else {
			$this->db->query("INSERT INTO `user` (`android_id`, `google_id`, `phone`, `expiry`) VALUES ('" . $androidID . "', '" . $googleID . "', '" . $phone . "', '" . $expiry . "')");
			$userID = intval($this->db->insert_id());
			echo json_encode(array(
				'response_code' => -1,
				'user_id' => $userID
			));
		}
	}
	
	public function complete_data() {
		$userID = intval($this->input->post('id'));
		$firstName = $this->input->post('first_name');
		$lastName = $this->input->post('last_name');
		$email = $this->input->post('email');
		$this->db->where('id', $userID);
		$this->db->update('user', array(
			'first_name' => $firstName,
			'last_name' => $lastName,
			'email' => $email,
			'profile_completed' => 1
		));
	}
	
	public function is_profile_completed() {
		$userID = intval($this->input->post('id'));
		$users = $this->db->query("SELECT * FROM `user` WHERE `id`=" . $userID)->result_array();
		if (sizeof($users) > 0) {
			$user = $users[0];
			echo json_encode(array(
				'profile_completed' => intval($user['profile_completed']),
				'user' => $user
			));
		} else {
			echo json_encode(array(
				'profile_completed' => 0
			));
		}
	}
	
	public function update_android_id() {
		$userID = intval($this->input->post('user_id'));
		$androidID = $this->input->post('android_id');
		$this->db->where('id', $userID);
		$this->db->update('user', array(
			'android_id' => $androidID
		));
	}
	
	public function get_gojek_token() {
		$phone = $this->input->post('phone');
		$user = $this->db->get_where('user', array(
			'phone' => $phone
		))->row_array();
		echo $user['gojek_token'];
	}
	
	public function get_grab_token() {
		$phone = $this->input->post('phone');
		$user = $this->db->get_where('user', array(
			'phone' => $phone
		))->row_array();
		echo $user['grab_token'];
	}
	
	public function get_token() {
		$setting = $this->db->query("SELECT * FROM `setting` LIMIT 1")->row_array();
		echo json_encode(array(
			'gojek_token' => $setting['gojek_token'],
			'grab_token' => $setting['grab_token']
		));
	}
	
	public function update_fcm_id() {
		$userID = intval($this->input->post('user_id'));
		$fcmID = $this->input->post('fcm_id');
		$this->db->where('id', $userID);
		$this->db->update('user', array(
			'fcm_id' => $fcmID
		));
	}
	
	public function check_user_expiry() {
		$users = $this->db->query("SELECT * FROM `user` WHERE `expiry`<NOW()")->result_array();
		for ($i=0; $i<sizeof($users); $i++) {
			$user = $users[$i];
			$fcmID = $user['fcm_id'];
			if ($fcmID != null && trim($fcmID) != "") {
				FCM::send_message("Masa berlaku akun Anda sudah habis", "Mohon hubungi admin untuk perpanjangan masa aktif akun", $fcmID, array('type' => 'logout'));
			}
		}
	}
	
	public function get_ticker() {
		echo json_encode($this->db->query("SELECT * FROM `tickers` LIMIT 1")->row_array());
	}
	
	public function verify_email() {
		$email = $this->input->post('email');
		$code = Util::generateRandomNumber(6);
		Util::send_email($email, 'Kode verifikasi email Anda: ' . $code, 'Mohon masukkan 6-digit kode verifikasi berikut di kotak yang tersedia di aplikasi: <b>' . $code . '</b>');
		echo $code;
	}
	
	public function get_settings() {
		echo json_encode($this->db->query("SELECT * FROM `setting` LIMIT 1")->row_array());
	}
	
	public function save_favorite_place() {
		$userID = $this->input->post('user_id');
		$title = $this->input->post('title');
		$address = $this->input->post('address');
		$lat = $this->input->post('lat');
		$lng = $this->input->post('lng');
		$date = $this->input->post('date');
		$this->db->insert('favorite_places', array(
			'user_id' => $userID,
			'title' => $title,
			'address' => $address,
			'lat' => $lat,
			'lng'=> $lng,
			'date' => $date
		));
	}
	
	public function get_favorite_places() {
		$userID = $this->input->post('user_id');
		echo json_encode($this->db->query("SELECT * FROM `favorite_places` WHERE `user_id`=" . $userID . " ORDER BY `date` DESC")
			->result_array());
	}
	
	public function clear_favorite_places() {
		$userID = $this->input->post('user_id');
		$this->db->query("DELETE FROM `favorite_places` WHERE `user_id`=" . $userID);
	}
	
	public function add_gofood_restaurant() {
		$currentDate = $this->input->post('current_date');
		$countryCode = $this->input->post('country_code');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$allRestaurants = json_decode($this->input->post('restaurants'), true);
		for ($i=0; $i<sizeof($allRestaurants); $i++) {
			$restaurant = $allRestaurants[$i];
			$lat = 0;
			$lng = 0;
			$restaurantID = "";
			if ($countryCode == "ID") {
				$location = $restaurant['content']['location'];
				$lat = doubleval(explode(",", $location)[0]);
				$lng = doubleval(explode(",", $location)[1]);
				$restaurantID = $restaurant['card_id'];
			} else if ($countryCode == "TH") {
				$location = $restaurant['content']['other_info']['location'];
				$lat = doubleval(explode(",", $location)[0]);
				$lng = doubleval(explode(",", $location)[1]);
				$restaurantID = $restaurant['card_template'];
			} else if ($countryCode == "VN") {
				$location = $restaurant['content']['other_info']['location'];
				$lat = doubleval(explode(",", $location)[0]);
				$lng = doubleval(explode(",", $location)[1]);
				$restaurantID = $restaurant['card_template'];
			}
			$this->db->query("DELETE FROM `gofood_restaurants` WHERE `restaurant_id`='".$restaurantID."'");
			$this->db->insert('gofood_restaurants', array(
				'restaurant_id' => $restaurantID,
				'country_code' => $countryCode,
				'tile_x' => $tileX,
				'tile_y' => $tileY,
				'lat' => $lat,
				'lng' => $lng,
				'data' => json_encode($restaurant),
				'last_update' => $currentDate
			));
		}
	}
	
	public function add_gofood_bs_restaurant() {
		$currentDate = $this->input->post('current_date');
		$countryCode = $this->input->post('country_code');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$allRestaurants = json_decode($this->input->post('restaurants'), true);
		for ($i=0; $i<sizeof($allRestaurants); $i++) {
			$restaurant = $allRestaurants[$i];
			$lat = 0;
			$lng = 0;
			$restaurantID = "";
			if ($countryCode == "ID") {
				$location = $restaurant['content']['location'];
				$lat = doubleval(explode(",", $location)[0]);
				$lng = doubleval(explode(",", $location)[1]);
				$restaurantID = $restaurant['card_id'];
			} else if ($countryCode == "TH") {
				$location = $restaurant['content']['other_info']['location'];
				$lat = doubleval(explode(",", $location)[0]);
				$lng = doubleval(explode(",", $location)[1]);
				$restaurantID = $restaurant['card_template'];
			} else if ($countryCode == "VN") {
				$location = $restaurant['content']['other_info']['location'];
				$lat = doubleval(explode(",", $location)[0]);
				$lng = doubleval(explode(",", $location)[1]);
				$restaurantID = $restaurant['card_template'];
			}
			$this->db->query("DELETE FROM `gofood_bs_restaurants` WHERE `restaurant_id`='".$restaurantID."'");
			$this->db->insert('gofood_bs_restaurants', array(
				'restaurant_id' => $restaurantID,
				'country_code' => $countryCode,
				'tile_x' => $tileX,
				'tile_y' => $tileY,
				'lat' => $lat,
				'lng' => $lng,
				'data' => json_encode($restaurant),
				'last_update' => $currentDate
			));
		}
	}
	
	public function add_gofood_mcd_restaurant() {
		$currentDate = $this->input->post('current_date');
		$countryCode = $this->input->post('country_code');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$allRestaurants = json_decode($this->input->post('restaurants'), true);
		for ($i=0; $i<sizeof($allRestaurants); $i++) {
			$restaurant = $allRestaurants[$i];
			$location = $restaurant['content']['other_info']['location'];
			$lat = doubleval(explode(",", $location)[0]);
			$lng = doubleval(explode(",", $location)[1]);
			$restaurantID = $restaurant['card_template'];
			$this->db->query("DELETE FROM `gofood_mcd_restaurants` WHERE `restaurant_id`='".$restaurantID."'");
			$this->db->insert('gofood_mcd_restaurants', array(
				'restaurant_id' => $restaurantID,
				'country_code' => $countryCode,
				'tile_x' => $tileX,
				'tile_y' => $tileY,
				'lat' => $lat,
				'lng' => $lng,
				'data' => json_encode($restaurant),
				'last_update' => $currentDate
			));
		}
	}
	
	public function add_grabfood_restaurant() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$allRestaurants = json_decode($this->input->post('restaurants'), true);
		for ($i=0; $i<sizeof($allRestaurants); $i++) {
			$restaurant = $allRestaurants[$i];
			$lat = $restaurant['latlng']['latitude'];
			$lng = $restaurant['latlng']['longitude'];
			$restaurantID = $restaurant['id'];
			$this->db->query("DELETE FROM `grabfood_restaurants` WHERE `restaurant_id`='".$restaurantID."'");
			$this->db->insert('grabfood_restaurants', array(
				'restaurant_id' => $restaurantID,
				'tile_x' => $tileX,
				'tile_y' => $tileY,
				'lat' => $lat,
				'lng' => $lng,
				'data' => json_encode($restaurant),
				'last_update' => $currentDate
			));
		}
	}
	
	public function add_grabfood_bs_restaurant() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$allRestaurants = json_decode($this->input->post('restaurants'), true);
		for ($i=0; $i<sizeof($allRestaurants); $i++) {
			$restaurant = $allRestaurants[$i];
			$lat = $restaurant['latlng']['latitude'];
			$lng = $restaurant['latlng']['longitude'];
			$restaurantID = $restaurant['id'];
			$this->db->query("DELETE FROM `grabfood_bs_restaurants` WHERE `restaurant_id`='".$restaurantID."'");
			$this->db->insert('grabfood_bs_restaurants', array(
				'restaurant_id' => $restaurantID,
				'tile_x' => $tileX,
				'tile_y' => $tileY,
				'lat' => $lat,
				'lng' => $lng,
				'data' => json_encode($restaurant),
				'last_update' => $currentDate
			));
		}
	}
	
	public function add_shopeefood_restaurant() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$allRestaurants = json_decode($this->input->post('restaurants'), true);
		for ($i=0; $i<sizeof($allRestaurants); $i++) {
			$restaurant = $allRestaurants[$i];
			$lat = doubleval($restaurant['store']['location']['latitude']);
			$lng = doubleval($restaurant['store']['location']['longitude']);
			$restaurantID = $restaurant['store']['id'];
			$this->db->query("DELETE FROM `grabfood_restaurants` WHERE `restaurant_id`='".$restaurantID."'");
			$this->db->insert('shopeefood_restaurants', array(
				'restaurant_id' => $restaurantID,
				'tile_x' => $tileX,
				'tile_y' => $tileY,
				'lat' => $lat,
				'lng' => $lng,
				'data' => json_encode($restaurant),
				'last_update' => $currentDate
			));
		}
	}
	
	public function add_traveloka_food_restaurant() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$allRestaurants = json_decode($this->input->post('restaurants'), true);
		for ($i=0; $i<sizeof($allRestaurants); $i++) {
			$restaurant = $allRestaurants[$i];
			$lat = 0; // latitude not available
			$lng = 0; // longitude not available
			$restaurantID = $restaurant['restaurantId'];
			$this->db->query("DELETE FROM `traveloka_food_restaurants` WHERE `restaurant_id`='".$restaurantID."'");
			$this->db->insert('traveloka_food_restaurants', array(
				'restaurant_id' => $restaurantID,
				'tile_x' => $tileX,
				'tile_y' => $tileY,
				'lat' => $lat,
				'lng' => $lng,
				'data' => json_encode($restaurant),
				'last_update' => $currentDate
			));
		}
	}
	
	public function add_traveloka_food_bs_restaurant() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$allRestaurants = json_decode($this->input->post('restaurants'), true);
		for ($i=0; $i<sizeof($allRestaurants); $i++) {
			$restaurant = $allRestaurants[$i];
			$lat = 0; // latitude not available
			$lng = 0; // longitude not available
			$restaurantID = $restaurant['restaurantId'];
			$this->db->query("DELETE FROM `traveloka_food_bs_restaurants` WHERE `restaurant_id`='".$restaurantID."'");
			$this->db->insert('traveloka_food_bs_restaurants', array(
				'restaurant_id' => $restaurantID,
				'tile_x' => $tileX,
				'tile_y' => $tileY,
				'lat' => $lat,
				'lng' => $lng,
				'data' => json_encode($restaurant),
				'last_update' => $currentDate
			));
		}
	}
	
	public function is_gofood_tile_exists() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `gofood_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		if (sizeof($restaurants) > 0) {
			$restaurant = $restaurants[0];
			$lastUpdate = $restaurant['last_update'];
			$diff = abs(strtotime($currentDate) - strtotime($lastUpdate));
			if ($diff > (30*24*60*60)) {
				echo -1;
				return;
			}
		}
		echo sizeof($restaurants);
	}
	
	public function is_gofood_bs_tile_exists() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `gofood_bs_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		if (sizeof($restaurants) > 0) {
			$restaurant = $restaurants[0];
			$lastUpdate = $restaurant['last_update'];
			$diff = abs(strtotime($currentDate) - strtotime($lastUpdate));
			if ($diff > (30*24*60*60)) {
				echo -1;
				return;
			}
		}
		echo sizeof($restaurants);
	}
	
	public function is_gofood_mcd_tile_exists() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `gofood_mcd_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		if (sizeof($restaurants) > 0) {
			$restaurant = $restaurants[0];
			$lastUpdate = $restaurant['last_update'];
			$diff = abs(strtotime($currentDate) - strtotime($lastUpdate));
			if ($diff > (30*24*60*60)) {
				echo -1;
				return;
			}
		}
		echo sizeof($restaurants);
	}
	
	public function is_grabfood_tile_exists() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `grabfood_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		if (sizeof($restaurants) > 0) {
			$restaurant = $restaurants[0];
			$lastUpdate = $restaurant['last_update'];
			$diff = abs(strtotime($currentDate) - strtotime($lastUpdate));
			if ($diff > (30*24*60*60)) {
				echo -1;
				return;
			}
		}
		echo sizeof($restaurants);
	}
	
	public function is_grabfood_bs_tile_exists() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `grabfood_bs_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		if (sizeof($restaurants) > 0) {
			$restaurant = $restaurants[0];
			$lastUpdate = $restaurant['last_update'];
			$diff = abs(strtotime($currentDate) - strtotime($lastUpdate));
			if ($diff > (30*24*60*60)) {
				echo -1;
				return;
			}
		}
		echo sizeof($restaurants);
	}
	
	public function is_shopeefood_tile_exists() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `shopeefood_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		if (sizeof($restaurants) > 0) {
			$restaurant = $restaurants[0];
			$lastUpdate = $restaurant['last_update'];
			$diff = abs(strtotime($currentDate) - strtotime($lastUpdate));
			if ($diff > (30*24*60*60)) {
				echo -1;
				return;
			}
		}
		echo sizeof($restaurants);
	}
	
	public function is_traveloka_food_tile_exists() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `traveloka_food_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		if (sizeof($restaurants) > 0) {
			$restaurant = $restaurants[0];
			$lastUpdate = $restaurant['last_update'];
			$diff = abs(strtotime($currentDate) - strtotime($lastUpdate));
			if ($diff > (30*24*60*60)) {
				echo -1;
				return;
			}
		}
		echo sizeof($restaurants);
	}
	
	public function is_traveloka_food_bs_tile_exists() {
		$currentDate = $this->input->post('current_date');
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `traveloka_food_bs_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		if (sizeof($restaurants) > 0) {
			$restaurant = $restaurants[0];
			$lastUpdate = $restaurant['last_update'];
			$diff = abs(strtotime($currentDate) - strtotime($lastUpdate));
			if ($diff > (30*24*60*60)) {
				echo -1;
				return;
			}
		}
		echo sizeof($restaurants);
	}
	
	public function get_gofood_restaurants() {
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `gofood_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		echo json_encode($restaurants);
	}
	
	public function get_grabfood_restaurants() {
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `grabfood_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		echo json_encode($restaurants);
	}
	
	public function get_shopeefood_restaurants() {
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `shopeefood_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		echo json_encode($restaurants);
	}
	
	public function get_traveloka_food_restaurants() {
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `traveloka_food_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		echo json_encode($restaurants);
	}
	
	public function get_traveloka_food_bs_restaurants() {
		$tileX = intval($this->input->post('tile_x'));
		$tileY = intval($this->input->post('tile_y'));
		$restaurants = $this->db->query("SELECT * FROM `traveloka_food_bs_restaurants` WHERE `tile_x`=".$tileX." AND `tile_y`=".$tileY)
			->result_array();
		echo json_encode($restaurants);
	}
}
