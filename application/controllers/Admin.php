<?php

class Admin extends CI_Controller {

	public function login() {
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$expiry = $this->input->post('expiry');
		$superAdmins = $this->db->query("SELECT * FROM `superadmin` WHERE `email`='" . $email . "' AND `password`='" . $password . "'")->result_array();
		if (sizeof($superAdmins) > 0) {
			$superAdmin = $superAdmins[0];
			echo json_encode(array(
				'response_code' => 1,
				'user_id' => intval($superAdmin['id']),
				'super_admin' => 1
			));
		} else {
			$admins = $this->db->query("SELECT * FROM `admin` WHERE `email`='" . $email . "' AND `password`='" . $password . "'")->result_array();
			if (sizeof($admins) > 0) {
				$admin = $admins[0];
				echo json_encode(array(
					'response_code' => 1,
					'user_id' => intval($admin['id']),
					'super_admin' => 0
				));
			} else {
				$admins = $this->db->query("SELECT * FROM `sub_admin` WHERE `email`='" . $email . "' AND `password`='" . $password . "'")->result_array();
				if (sizeof($admins) > 0) {
					$admin = $admins[0];
					echo json_encode(array(
						'response_code' => 1,
						'user_id' => intval($admin['id']),
						'super_admin' => 0,
						'sub_admin' => 1
					));
				} else {
					echo json_encode(array(
						'response_code' => -2
					));
				}
			}
		}
	}
	
	public function add_user() {
		$adminID = intval($this->input->post('admin_id'));
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$password = $this->input->post('password');
		$androidID = $this->input->post('android_id');
		$expiry = $this->input->post('expiry');
		$slotsUsed = intval($this->input->post('slots_used'));
		$date = $this->input->post('date');
		$maxUsers = intval($this->db->query("SELECT * FROM `admin` WHERE `id`=" . $adminID)->row_array()['max_users']);
		$users = $this->db->query("SELECT * FROM `user` WHERE TRIM(`email`)!='' AND TRIM(`email`)='" . $email . "'")->result_array();
		if (sizeof($users) > 0) {
			echo json_encode(array('response_code' => -2, 'max_users' => $maxUsers));
			return;
		}
		if ($maxUsers <= 0) {
			echo json_encode(array('response_code' => -1, 'max_users' => $maxUsers));
			return;
		}
		if ($maxUsers > 0) {
			if ($maxUsers >= $slotsUsed) {
				$this->db->query("INSERT INTO `user` (`admin_id`, `email`, `phone`, `password`, `android_id`, `expiry`) VALUES (" . $adminID . ", '" . $email . "', '" . $phone . "', '" . $password . "', '" . $androidID . "', '" . $expiry . "')");
				$userID = intval($this->db->insert_id());
				$maxUsers -= $slotsUsed;
				$this->db->query("UPDATE `admin` SET `max_users`=" . $maxUsers . " WHERE `id`=" . $adminID);
				$this->db->insert('activity', array(
					'admin_id' => $adminID,
					'user_id' => $userID,
					'type' => 'add_user',
					'slots' => -1,
					'date' => $date
				));
				echo json_encode(array('response_code' => 1, 'user_id' => $userID, 'max_users' => $maxUsers));
			} else {
				echo json_encode(array('response_code' => -1));
				return;
			}
		}
	}
	
	public function add_sub_admin_user() {
		$subAdminID = intval($this->input->post('sub_admin_id'));
		$email = $this->input->post('email');
		$phone = $this->input->post('phone');
		$password = $this->input->post('password');
		$androidID = $this->input->post('android_id');
		$expiry = $this->input->post('expiry');
		$slotsUsed = intval($this->input->post('slots_used'));
		$date = $this->input->post('date');
		$maxUsers = intval($this->db->query("SELECT * FROM `sub_admin` WHERE `id`=" . $subAdminID)->row_array()['max_users']);
		$users = $this->db->query("SELECT * FROM `user` WHERE `email`='" . $email . "'")->result_array();
		if (sizeof($users) < 0) {
			echo json_encode(array('response_code' => -2, 'max_users' => $maxUsers));
			return;
		}
		if ($maxUsers <= 0) {
			echo json_encode(array('response_code' => -1, 'max_users' => $maxUsers));
			return;
		}
		if ($maxUsers > 0) {
			if ($maxUsers >= $slotsUsed) {
				$this->db->query("INSERT INTO `user` (`sub_admin_id`, `email`, `phone`, `password`, `android_id`, `expiry`) VALUES (" . $subAdminID . ", '" . $email . "', '" . $phone . "', '" . $password . "', '" . $androidID . "', '" . $expiry . "')");
				$userID = intval($this->db->insert_id());
				$maxUsers -= $slotsUsed;
				$this->db->query("UPDATE `sub_admin` SET `max_users`=" . $maxUsers . " WHERE `id`=" . $subAdminID);
				$this->db->insert('activity', array(
					'sub_admin_id' => $subAdminID,
					'user_id' => $userID,
					'type' => 'add_sub_admin_user',
					'slots' => -1,
					'date' => $date
				));
				echo json_encode(array('response_code' => 1, 'user_id' => $userID, 'max_users' => $maxUsers));
			} else {
				echo json_encode(array('response_code' => -1));
				return;
			}
		}
	}

	public function get_users() {
		$adminID = intval($this->input->post('admin_id'));
		$start = intval($this->input->post('start'));
		$length = intval($this->input->post('length'));
		//$users = $this->db->query("SELECT * FROM `user` WHERE `admin_id`=" . $adminID . " ORDER BY `first_name` ASC LIMIT " . $start . "," . $length)->result_array();
		$users = $this->db->query("SELECT * FROM `user` WHERE `admin_id`=" . $adminID . " ORDER BY `first_name` ASC")->result_array();
		for ($i=0; $i<sizeof($users); $i++) {
			$row = $this->db->query("SELECT * FROM `admin` WHERE `id`=" . $users[$i]['admin_id'])->row_array();
			$users[$i]['admin_name'] = $row['name'];
		}
		echo json_encode($users);
	}

	public function get_sub_admin_users() {
		$subAdminID = intval($this->input->post('sub_admin_id'));
		$start = intval($this->input->post('start'));
		$length = intval($this->input->post('length'));
		//$users = $this->db->query("SELECT * FROM `user` WHERE `admin_id`=" . $subAdminID . " ORDER BY `first_name` ASC LIMIT " . $start . "," . $length)->result_array();
		$users = $this->db->query("SELECT * FROM `user` WHERE `sub_admin_id`=" . $subAdminID . " ORDER BY `first_name` ASC")->result_array();
		for ($i=0; $i<sizeof($users); $i++) {
			$row = $this->db->query("SELECT * FROM `admin` WHERE `id`=" . $users[$i]['admin_id'])->row_array();
			$users[$i]['admin_name'] = $row['name'];
		}
		echo json_encode($users);
	}
	
	public function get_all_users() {
		$start = intval($this->input->post('start'));
		$length = intval($this->input->post('length'));
		//$users = $this->db->query("SELECT * FROM `user` ORDER BY `first_name` ASC LIMIT " . $start . "," . $length)->result_array();
		$users = $this->db->query("SELECT * FROM `user` ORDER BY `first_name` ASC")->result_array();
		for ($i=0; $i<sizeof($users); $i++) {
			$row = $this->db->query("SELECT * FROM `admin` WHERE `id`=" . $users[$i]['admin_id'])->row_array();
			$users[$i]['admin_name'] = $row['name'];
		}
		echo json_encode($users);
	}

	public function get_admins() {
		$start = intval($this->input->post('start'));
		$length = intval($this->input->post('length'));
		//$admins = $this->db->query("SELECT * FROM `admin` ORDER BY `email` ASC LIMIT " . $start . "," . $length)->result_array();
		$admins = $this->db->query("SELECT * FROM `admin` ORDER BY `email` ASC")->result_array();
		for ($i=0; $i<sizeof($admins); $i++) {
			$admins[$i]['user_count'] = $this->db->query("SELECT * FROM `user` WHERE `admin_id`=" . $admins[$i]['id'])->num_rows();
		}
		echo json_encode($admins);
	}
	
	public function set_user_suspended() {
		$adminID = intval($this->input->post('admin_id'));
		$userID = intval($this->input->post('user_id'));
		$suspended = intval($this->input->post('suspended'));
		$date = $this->input->post('date');
		$this->db->where('id', $userID);
		$this->db->update('user', array(
			'suspended' => $suspended
		));
		$this->db->insert('activity', array(
			'admin_id' => $adminID,
			'user_id' => $userID,
			'type' => $suspended==0?'reenable_user':'suspend_user',
			'date' => $date
		));
	}
	
	public function delete_admin() {
		$adminID = intval($this->input->post('admin_id'));
		$deletedAdminID = intval($this->input->post('deleted_admin_id'));
		$date = $this->input->post('date');
		$this->db->query("DELETE FROM `admin` WHERE `id`=" . $deletedAdminID);
		$this->db->insert('activity', array(
			'admin_id' => $adminID,
			'user_id' => $deletedAdminID,
			'type' => 'delete_admin',
			'date' => $date
		));
	}
	
	public function delete_user() {
		$adminID = intval($this->input->post('admin_id'));
		$userID = intval($this->input->post('user_id'));
		$date = $this->input->post('date');
		$this->db->query("DELETE FROM `user` WHERE `id`=" . $userID);
		$this->db->insert('activity', array(
			'admin_id' => $adminID,
			'user_id' => $userID,
			'type' => 'delete_user',
			'date' => $date
		));
	}
	
	public function add_admin() {
		$adminID = intval($this->input->post('admin_id'));
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$maxUsers = intval($this->input->post('max_users'));
		$date = $this->input->post('date');
		$this->db->insert('admin', array(
			'name' => $name,
			'email' => $email,
			'password' => $password,
			'max_users' => $maxUsers
		));
		$addedAdminID = intval($this->db->insert_id());
		$this->db->insert('activity', array(
			'admin_id' => $adminID,
			'user_id' => $addedAdminID,
			'type' => 'add_admin',
			'date' => $date
		));
	}
	
	public function update_admin() {
		$adminID = intval($this->input->post('admin_id'));
		$editedAdminID = intval($this->input->post('edited_admin_id'));
		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$maxUsers = intval($this->input->post('max_users'));
		$slots = intval($this->input->post('slots'));
		$date = $this->input->post('date');
		$this->db->where('id', $editedAdminID);
		$this->db->update('admin', array(
			'name' => $name,
			'email' => $email,
			'password' => $password,
			'max_users' => $maxUsers
		));
		$this->db->insert('activity', array(
			'admin_id' => $adminID,
			'user_id' => $editedAdminID,
			'type' => 'modify_slots',
			'slots' => $slots,
			'date' => $date
		));
	}
	
	public function update_user() {
		$adminID = intval($this->input->post('admin_id'));
		$userID = intval($this->input->post('user_id'));
		$phone = $this->input->post('phone');
		$password = $this->input->post('password');
		$androidID = $this->input->post('android_id');
		$expiry = $this->input->post('expiry');
		$maxUsers = intval($this->input->post('max_users'));
		$slotsUsed = intval($this->input->post('slots_used'));
		$this->db->where('id', $userID);
		$this->db->update('user', array(
			'phone' => $phone,
			'password' => $password,
			'android_id' => $androidID,
			'expiry' => $expiry,
			'max_users' => $maxUsers
		));
		$this->db->insert('activity', array(
			'admin_id' => $adminID,
			'user_id' => $userID,
			'type' => 'modify_slot',
			'slots' => $slotsUsed,
			'date' => $date
		));
	}
	
	public function update_gojek_token() {
		$token = $this->input->post('token');
		$this->db->update('setting', array(
			'gojek_token' => $token
		));
	}
	
	public function update_grab_token() {
		$token = $this->input->post('token');
		$this->db->update('setting', array(
			'grab_token' => $token
		));
	}
	
	public function update_settings() {
		$version = $this->input->post('version');
		$downloadLink = $this->input->post('download_link');
		$gojekToken = $this->input->post('gojek_token');
		$grabToken = $this->input->post('grab_token');
		$this->db->update('setting', array(
			'version' => $version,
			'download_link' => $downloadLink,
			'gojek_token' => $gojekToken,
			'grab_token' => $grabToken
		));
	}
	
	public function get_token() {
		$setting = $this->db->query("SELECT * FROM `setting` LIMIT 1")->row_array();
		echo json_encode(array(
			'gojek_token' => $setting['gojek_token'],
			'grab_token' => $setting['grab_token']
		));
	}
	
	public function get_sub_admins() {
		$adminID = intval($this->input->post('admin_id'));
		$start = intval($this->input->post('start'));
		$length = intval($this->input->post('length'));
		$subadmins = $this->db->query("SELECT * FROM `sub_admin` WHERE `admin_id`=" . $adminID . " LIMIT " . $start . "," . $length)->result_array();
		for ($i=0; $i<sizeof($subadmins); $i++) {
			$subadmins[$i]['total_user'] = $this->db->query("SELECT * FROM `user` WHERE `sub_admin_id`=" . $subadmins[$i]['id'])->num_rows();
		}
		echo json_encode($subadmins);
	}
	
	public function add_sub_admin_gem() {
		$adminID = intval($this->input->post('admin_id'));
		$subAdminID = intval($this->input->post('sub_admin_id'));
		$gem = intval($this->input->post('max_users'));
		$date = $this->input->post('date');
		$isSuperadmin = intval($this->input->post('is_superadmin'));
		$this->db->query("UPDATE `sub_admin` SET `max_users`=`max_users`+" . $gem . " WHERE `id`=" . $subAdminID . "");
		$this->db->insert('activity', array(
			'admin_id' => $adminID,
			'user_id' => $subAdminID,
			'is_superadmin' => $isSuperadmin,
			'type' => 'add_sub_admin_gem',
			'slots' => $gem,
			'date' => $date
		));
	}
	
	public function update_ticker() {
		$content = $this->input->post('content');
		$tickers = $this->db->query("SELECT * FROM `tickers` LIMIT 1")->result_array();
		if (sizeof($tickers) > 0) {
			$this->db->query("UPDATE `tickers` SET `content`='" . $content . "'");
		} else {
			$this->db->insert('tickers', array(
				'content' => $content
			));
		}
	}
	
	public function get_settings() {
		echo json_encode($this->db->query("SELECT * FROM `setting` LIMIT 1")->row_array());
	}
	
	public function get_histories() {
		$adminID = intval($this->input->post('admin_id'));
		$subAdminID = intval($this->input->post('sub_admin_id'));
		$start = intval($this->input->post('start'));
		$length = intval($this->input->post('length'));
		$adminName = "";
		if ($adminID == 0 && $subAdminID != 0) {
			$subAdmin = $this->db->query("SELECT * FROM `sub_admin` WHERE `id`=" . $subAdminID)->row_array();
			if ($subAdmin != NULL) {
				$adminName = $subAdmin['name'];
			}
		} else if ($adminID != 0 && $subAdminID == 0) {
			$admin = $this->db->query("SELECT * FROM `admin` WHERE `id`=" . $adminID)->row_array();
			if ($admin != NULL) {
				$adminName = $admin['name'];
			}
		}
		$histories = $this->db->query("SELECT * FROM `activity` WHERE `admin_id`=" . $adminID . " OR `sub_admin_id`=" . $subAdminID . " ORDER BY `date` DESC LIMIT " . $start . "," . $length)->result_array();
		for ($i=0; $i<sizeof($histories); $i++) {
			$history = $histories[$i];
			$type = $history['type'];
			$userName = "";
			if ($history['user_id'] != NULL && intval($history['user_id']) != 0) {
				if ($type == 'add_user') {
					$user = $this->db->query("SELECT * FROM `user` WHERE `id`=" . $history['user_id'])->row_array();
					if ($user != NULL) $userName = $user['first_name'] . " " . $user['last_name'];
				} else if ($type == 'reenable_user') {
					$user = $this->db->query("SELECT * FROM `user` WHERE `id`=" . $history['user_id'])->row_array();
					if ($user != NULL) $userName = $user['first_name'] . " " . $user['last_name'];
				} else if ($type == 'suspend_user') {
					$user = $this->db->query("SELECT * FROM `user` WHERE `id`=" . $history['user_id'])->row_array();
					if ($user != NULL) $userName = $user['first_name'] . " " . $user['last_name'];
				} else if ($type == 'delete_admin') {
					$user = $this->db->query("SELECT * FROM `admin` WHERE `id`=" . $history['user_id'])->row_array();
					if ($user != NULL) $userName = $user['name'];
				} else if ($type == 'add_admin') {
					$user = $this->db->query("SELECT * FROM `admin` WHERE `id`=" . $history['user_id'])->row_array();
					if ($user != NULL) $userName = $user['name'];
				} else if ($type == 'edit_admin') {
					$user = $this->db->query("SELECT * FROM `admin` WHERE `id`=" . $history['user_id'])->row_array();
					if ($user != NULL) $userName = $user['name'];
				} else if ($type == 'delete_sub_admin') {
					$user = $this->db->query("SELECT * FROM `sub_admin` WHERE `id`=" . $history['user_id'])->row_array();
					if ($user != NULL) $userName = $user['name'];
				} else if ($type == 'add_sub_admin_user') {
					$user = $this->db->query("SELECT * FROM `sub_admin` WHERE `id`=" . $history['user_id'])->row_array();
					if ($user != NULL) $userName = $user['name'];
				} else if ($type == 'edit_sub_admin') {
					$user = $this->db->query("SELECT * FROM `sub_admin` WHERE `id`=" . $history['user_id'])->row_array();
					if ($user != NULL) $userName = $user['name'];
				} else if ($type == 'modify_slots') {
					$user = $this->db->query("SELECT * FROM `user` WHERE `id`=" . $history['user_id'])->row_array();
					if ($user != NULL) $userName = $user['name'];
				} else if ($type == 'add_sub_admin_gem') {
					$admin = $this->db->query("SELECT * FROM `sub_admin` WHERE `id`=" . $history['user_id'])->row_array();
					if ($admin != NULL) $userName = $admin['name'];
				}
			}
			$isSuperadmin = intval($history['is_superadmin']);
			if ($isSuperadmin == 0) {
				if ($history['admin_id'] != NULL) {
					$admin = $this->db->query("SELECT * FROM `admin` WHERE `id`=" . $history['admin_id'])->row_array();
					$adminName = $admin['name'];
				}
			} else {
				if ($history['admin_id'] != NULL) {
					$admin = $this->db->query("SELECT * FROM `superadmin` WHERE `id`=" . $history['admin_id'])->row_array();
					$adminName = $admin['name'];
				}
			}
			$histories[$i]['admin_name'] = $adminName;
			$histories[$i]['user_name'] = $userName;
		}
		echo json_encode($histories);
	}
}
